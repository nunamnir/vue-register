// файл vue.config.js расположен в корне вашего репозитория
module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  publicPath: process.env.NODE_ENV === 'production' ? '/vue-register/' : '/'
};